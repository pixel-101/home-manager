{
  imports = [ ./home ];
  home = {
    username = "deck";
    homeDirectory = "/home/deck";
    stateVersion = "23.11";
  };
}
