let
  pfp = "stock_okuu.jpeg";
  wallpaper = "subterranean_sun.jpeg";
in
{
  home.file.".face".source = ./resources/user-pictures/faces + ("/" + pfp);

  xdg.configFile = {
    "wallpaper".source = ./resources/user-pictures/wallpapers + ("/" + wallpaper);
    "wallpaper-dark".source = ./resources/user-pictures/wallpapers + ("/" + wallpaper);
  };
  home.file.".background-image".source = ./resources/user-pictures/wallpapers + ("/" + wallpaper);
}
