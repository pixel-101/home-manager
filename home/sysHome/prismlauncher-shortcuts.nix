{ config, lib, pkgs, ... }:
with lib.attrsets;
let
  cfg.instances = [
    "GregTech- New Horizons"
  ];
in
{
  xdg.desktopEntries."GregTech- New Horizons" = {
    exec = "${pkgs.prismlauncher}/bin/prismlauncher --launch \"GregTech- New Horizons\"";
    icon = "${config.xdg.dataHome}/PrismLauncher/instances/GregTech- New Horizons/icon.png";
    name = "GregTech- New Horizons";
  };
}
