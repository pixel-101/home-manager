{
  programs.bash = {
    enable = true;
    historyFile = "/nix/state/home/pixel/.local/share/bash/history";
    enableVteIntegration = true;
    enableCompletion = true;
  };

  programs.zoxide = {
    enable = true;
    options = [ "--cmd=cd" ];
  };
}
