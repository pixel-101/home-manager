{ lib, pkgs, ... }:
{
  home.packages = with pkgs; [
    craftos-pc
    dosbox-x
    protontricks
    bottles
    spot
    # komikku # package is borked atm... using flatpak until fixed
    prismlauncher-qt5
    libreoffice
    taisei
    protonup-qt
    r2modman
    syncthing
    stc-cli
    youtube-music
    steam-rom-manager
    hunspell
    hunspellDicts.en_US-large
    delfin
    wineWowPackages.waylandFull
    osu-lazer-bin
    vesktop
    grapejuice
    bat
    glow
    gpu-screen-recorder-gtk
    file
    kdenlive
    dar
  ];
}
