{ lib, pkgs, ... }:
with lib;
{
  # Blatently stolen from targets.genericLinux
  xdg.systemDirs.data = [
    # Nix profiles
    "\${NIX_STATE_DIR:-/nix/var/nix}/profiles/default/share"
    "${home.profileDirectory}/share"
    
    # Distro Data
    "${xdg.dataHome}/flatpak/exports/share"
    "/var/lib/flatpak/exports/share"
    "/usr/local/share"
    "/usr/share"
  ];
  # We need to append system-wide FHS directories due to the default prefix
  # resolving to the Nix store.
  # https://github.com/nix-community/home-manager/pull/2891#issuecomment-1101064521
  home.sessionVariables = {
    XCURSOR_PATH = "$XCURSOR_PATH\${XCURSOR_PATH:+:}" + concatStringsSep ":" [
      "${config.home.profileDirectory}/share/icons"
      "/usr/share/icons"
      "/usr/share/pixmaps"
    ];
  };
  # We need to source both nix.sh and hm-session-vars.sh as noted in
  # https://github.com/nix-community/home-manager/pull/797#issuecomment-544783247
  programs.bash.initExtra = ''
    . "${nixPkg}/etc/profile.d/nix.sh"
    . "${profileDirectory}/etc/profile.d/hm-session-vars.sh"
  '';

  systemd.user.sessionVariables = {
    NIX_PATH = "$HOME/.nix-defexpr/channels\${NIX_PATH:+:}$NIX_PATH";
    TERMINFO_DIRS = "${home.profileDirectory}/share/terminfo:$TERMINFO_DIRS\${TERMINFO_DIRS:+:}/usr/share/terminfo";
  };
}
