{
  # Remove the annoying SD flatpak "thing" for firefox
  xdg.desktopEntries."org.mozilla.firefox".noDisplay = true;
}
