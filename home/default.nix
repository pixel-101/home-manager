{ config, ... }:
{
  imports = [
    # Custom modules
    ./syncthing.nix
    ./path.nix
    ./nix.nix
    ./no-flatpak.nix

    # Existing modules
    ./sysHome/editor.nix
    ./sysHome/firefox.nix
    ./sysHome/git.nix
    ./sysHome/kodi.nix
    ./sysHome/packages.nix
    ./sysHome/prismlauncher-shortcuts.nix
    ./sysHome/shell.nix
    ./sysHome/syncthing-ignore.nix { deckCfg = config; }
  ];
  programs.home-manager.enable = true;
}
